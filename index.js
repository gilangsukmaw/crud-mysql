const express = require("express");

const app = express();
const port = 5000;

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

//routes
const customersRoutes = require("./routes/customers");
const suppliersRoutes = require("./routes/suppliers");
const goodsRoutes = require("./routes/goods");

app.use("/customers", customersRoutes);
app.use("/suppliers", suppliersRoutes);
app.use("/goods", goodsRoutes);

app.listen(port, () => {
  console.log(`server running on port ${port}`);
});
