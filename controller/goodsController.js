const connection = require("../models");

class Goods {
  //Get all good
  getAllGoods = async (req, res) => {
    try {
      const data = await connection.query(
        `SELECT g.id as good_id, g.name as good_name, g.price as good_price, s.name as supplier
          FROM goods g
          JOIN suppliers s ON g.id_supplier = s.id
          ORDER BY g.id
          `
      );

      if (data.length === 0) {
        return res.status(404).json({
          message: "No data found!",
        });
      }
      return res.status(200).json({
        data,
      });
    } catch (error) {
      res.status(500).json({
        message: error,
      });
    }
  };

  //Get by Id
  getById = async (req, res) => {
    try {
      const data = await connection.query(`
          SELECT g.id as good_id, g.name as good_name, g.price as good_price, s.name as supplier
          FROM goods g
          JOIN suppliers s ON g.id_supplier = s.id
          WHERE g.id = ${req.params.id}
          `);

      if (data.length === 0) {
        return res.status(404).json({
          message: "No good with that id!",
        });
      }
      return res.status(200).json({
        data,
      });
    } catch (error) {
      res.status(500).json({
        message: error,
      });
    }
  };

  //Add good
  addGood = async (req, res) => {
    try {
      const findSup = await connection.query(`
          SELECT name FROM suppliers WHERE id = ${req.body.supplier_id}
          `);

      if (findSup.length === 0) {
        return res.status(404).json({
          message: "No supplier with that id!",
        });
      }

      const insGood = await connection.query(`
      INSERT INTO goods (name, price, id_supplier) 
      VALUES 
        ( '${req.body.name}', 
          ${req.body.price}, 
          ${req.body.supplier_id}
        )
      `);

      const data = await connection.query(`
          SELECT g.id as good_id, g.name as good_name, g.price as good_price, s.name as supplier
          FROM goods g
          JOIN suppliers s ON g.id_supplier = s.id
          WHERE g.id = ${insGood.insertId}
        `);

      return res.status(201).json({
        message: "Good added!",
        data,
      });
    } catch (error) {
      res.status(500).json({
        message: error,
      });
    }
  };

  //Update good
  updateGood = async (req, res) => {
    try {
      //find Supplier
      const findSup = await connection.query(`
          SELECT name FROM suppliers WHERE id = ${req.body.supplier_id}
      `);

      if (findSup.length === 0) {
        return res.status(404).json({
          message: "No supplier with that id!",
        });
      }

      // find the good
      const findGood = await connection.query(`
          SELECT * FROM goods WHERE id = ${req.params.id}
          `);

      if (findGood.length === 0) {
        return res.status(404).json({
          message: "No good with that id!",
        });
      }

      //Updating good
      const updateGood = await connection.query(`
      UPDATE goods
        SET
          name='${req.body.name}',
          price=${req.body.price},
          id_supplier=${req.body.supplier_id}
        WHERE id=${req.params.id}
      `);

      //find updated good
      const data = await connection.query(`
          SELECT g.id as good_id, g.name as good_name, g.price as good_price, s.name as supplier
          FROM goods g
          JOIN suppliers s ON g.id_supplier = s.id
          WHERE g.id = ${req.params.id}
        `);

      return res.status(201).json({
        message: "Good updated!",
        data,
      });
    } catch (error) {
      res.status(500).json({
        message: error,
      });
    }
  };

  //Delete good
  deleteGood = async (req, res) => {
    const data = await connection.query(`
    DELETE FROM goods WHERE id=${req.params.id}
    `);

    if (data.affectedRows === 0) {
      return res.status(404).json({
        message: "No good with that id!",
      });
    }

    return res.status(200).json({
      message: `Yea bois, Good with ID ${req.params.id} deleted`,
    });
  };
}

module.exports = new Goods();
