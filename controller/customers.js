const { query } = require("../models/index");

class customers {
  async getAllCustomers(req, res) {
    try {
      const data = await query("SELECT * FROM customers");

      if (data.length === 0) {
        return res.status(404).json({
          message: "Customer not found",
        });
      }

      res.status(200).json({
        data,
      });
    } catch (error) {
      return res.status(500).json({
        message: error.message,
      });
    }
  }

  async getDetailCustomer(req, res) {
    try {
      const data = await query(
        `SELECT * FROM customers WHERE id=${req.params.id}`
      );

      if (data.length === 0) {
        return res.status(404).json({
          message: "Customer not found",
        });
      }

      res.status(200).json({
        data,
      });
    } catch (error) {
      return res.status(500).json({
        message: error.message,
      });
    }
  }

  async createCustomer(req, res) {
    try {
      /* Insert customers */
      const newCustomer = await query(
        `INSERT INTO customers (name) VALUES ('${req.body.name}')`
      );

      /* Get the newCustomer data */
      const data = await query(
        `SELECT * FROM customers WHERE id=${newCustomer.insertId}`
      );

      res.status(201).json({
        data,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async updateCustomer(req, res) {
    try {
      /* Find the customers */
      const customer = await query(
        `SELECT * FROM customers WHERE id=${req.params.id}`
      );

      if (customer.length === 0) {
        return res.status(404).json({
          message: "Customer not found",
        });
      }

      /* Update customers */
      await query(
        `UPDATE customers SET name='${req.body.name}' WHERE id=${req.params.id}`
      );

      /* Get the updated customers data */
      const data = await query(
        `SELECT * FROM customers WHERE id=${req.params.id}`
      );

      res.status(201).json({
        data,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async deleteCustomer(req, res) {
    try {
      const deletedData = await query(
        `DELETE FROM customers WHERE id=${req.params.id}`
      );

      if (deletedData.affectedRows === 0) {
        return res.status(404).json({
          message: "Customers not found",
        });
      }

      res.status(200).json({
        message: `Customers with id ${req.params.id} has been deleted`,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
}

module.exports = new customers();
