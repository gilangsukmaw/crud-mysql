const mysql = require("mysql2");
const { promisify } = require("util");
//dotenv
require("dotenv").config();

//db connection
const connection = mysql.createConnection({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  multipleStatements: true,
});

//connection connect
connection.connect((err) => {
  if (err) {
    console.log(err);
  }
  console.log("Connected to MySQL DB");
});

exports.query = promisify(connection.query).bind(connection);
